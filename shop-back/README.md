# Back-end

## Getting Started

    npm install
    npm run dev || yarn dev

## Routes

<details>
<summary>Web Tournament</summary>

#### `/api/tournament/create`
     String : tournament_name
     String : description
     Int : fee
     String : bracket_type (ex. "single", "double")
     Int: number_of_teams
     String: type (ex. "BO1", "BO3")
     String: t_type (ex. "1vs1", "5vs5")
     Int: hero ("hero_id")
     String: tournament_start_date (etc. "Tue Feb 23 2021 11:32:22 GMT+0800 (Ulaanbaatar Standard Time)")
#### `/api/tournament/checkin`
     String : team_id
     String : t_id (tournament id)
#### `/api/tournament/register`
     String : team_id
     String : t_id (tournament id)
#### `/api/tournament/register/cancel`
     String : team_id
     String : t_id (tournament id)
#### `/api/cointoss/ready`
     String : match_id
     String : team_name
#### `/api/cointoss/result`
     String : match_id
     String : winner_team (winner team name)
#### `/api/cointoss/pick`
     String : match_id
     String : team_name
     String : pick (ex 'dire', 'radiant', 'first', 'last')


<summary>Tournament response codes</summary>

#### `create`
     0: created successfully
     
#### `register`
     0: Need team_name and tournament_name.
     1: You aren't the captain of the team.
     2: Not enough team members.
     3: Tournament doesn't exist.
     4: Registration is full.
     5: The tournament is already started.
     6: The team is already registered in this tournament.
     7: Successfully registered. (error: false)
     8: User is already registered in this tournament.

#### `register/cancel`
     0: Need team_name and tournament_name.
     1: You aren't the captain of the team.
     2: The team is not registered.
     3: Successfully canceled. (error: false)

</details>


<details>

<summary>User</summary>

#### `/api/user/register`

     String : email
     String : password
     String : confirm_password
     String : username
     String : display_name

#### `/api/user/update/name`

     String : display_name

#### `/api/user/update/picture/profile`

     String : x
     String : y
     String : width
     String : height
     File : image

#### `/api/user/update/email`

     String : email

#### `/api/user/update/password`

     String : password
     String : email

#### `/api/user/update/picture/cover`

     String : x
     String : y
     String : width
     String : height
     File : image

#### `/api/user/update/game`

     String : game (ex. 'dota', 'csgo)

#### `/api/user/old/login` (old users have field value "old_hashed_password")

     String : email
     String : password


</details>

<details>
<summary>Team</summary>

#### `/api/team/create`

     String : team_name

#### `/api/team/invite/send`

     String : team_id
     String : user_id

#### `/api/team/invite/accept`

     String : team_id

#### `/api/team/invite/cancel`

     String : team_id
     String : user_id

#### `/api/team/invite/decline`

     String : team_id

#### `/api/team/change/captain`

     String : team_id
     String : user_id

#### `/api/team/change/name`

     String : team_id
     String : new_team_name


#### `/api/team/change/picture/cover`

     String : x
     String : y
     String : width
     String : height
     File : image
     String : team_id

#### `/api/team/change/picture/profile`

     String : x
     String : y
     String : width
     String : height
     File : image
     String : team_id

#### `/api/team/member/kick`

     String : team_id
     String : user_id


#### `/api/team/leave`

     String : team_id

#### `/api/team/delete`

     String : team_id
     
#### `/api/team/page/first`

#### `/api/team/page/next`
     
     String : last_doc

#### `/api/team/page/prev`

     String : first_doc

</details>

<details>
<summary>Chat</summary>

#### `/api/chat/send`

     String : message

#### `/api/chat/team/send`

     String : team_id
     String : message


</details>
