import admin from 'firebase-admin'
const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://shop-8aa2c.firebaseio.com",
  storageBucket : 'shop-8aa2c.appspot.com'
});

export default admin;