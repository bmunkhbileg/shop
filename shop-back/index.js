import express from 'express'
const app = express();
import cors from 'cors'
require("dotenv").config();

//import routes
import product from './routes/product'

//other
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//routes
product(app);


app.listen(process.env.PORT || 5001, () => console.log("Server running on http://localhost:" + (process.env.PORT || 5001)));
