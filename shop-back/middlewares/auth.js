import admin from '../firebase/firebase'
const auth = admin.auth()

//check if user is logged in
export const isAuthenticated = async (req, res, next) => {
    const header = req.headers.authorization
    if(header) {
      const idToken = header.split('Bearer ')[1];
      try {
        const decodedToken = await auth.verifyIdToken(idToken);
        req.user = decodedToken;
        next();
      } catch (err) {
        if(err.errorInfo.message) {
          res.status(401).json({
            message : err.errorInfo.message
          })
        } else {
          res.status(401).json(err)
        }
      } 
    } else {
      res.status(401).json({
        message : "Token does not exist"
      })
    }
  }

// // check if user is an Admin
// const isAdmin = async (req, res, next) => {
//   if (req.headers.authorization.startsWith('Bearer ')) {
//       const idToken = req.headers.authorization.split('Bearer ')[1];
//       try {
//         const decodedToken = await auth.verifyIdToken(idToken);
//         await users.doc(decodedToken.uid).get().then(snapshot => {
//           if(snapshot.exists) {
//               let role = snapshot.data().role;
//               if(role === "admin") {
//                   next();
//               } else {
//                 res.status(422).json({
//                   message : "Unauthourized"
//                 })
//               }
//           } else {
//             res.status(422).json({
//               message : "user does not exist"
//             })
//           }
//         })        
//       } catch (err) {
//         console.log(err);
//       }
//     }
// }

// // check if user is an organiser
// const isOrganiser = async (req, res, next) => {
//   if (req.headers.authorization.startsWith('Bearer ')) {
//       const idToken = req.headers.authorization.split('Bearer ')[1];
//       try {
//         const decodedToken = await auth.verifyIdToken(idToken);
//         await users.doc(decodedToken.uid).get().then(snapshot => {
//           if(snapshot.exists) {
//               let role = snapshot.data().role;
//               if(role === "organiser") {
//                   next();
//               } else {
//                 res.status(422).json({
//                   message : "Unauthourized"
//                 })
//               }
//           } else {
//             res.status(422).json({
//               message : "user does not exist"
//             })
//           }
//         })        
//       } catch (err) {
//         console.log(err);
//       }
//     }
// }
