import admin from '../firebase/firebase'
const firestore = admin.firestore()
const users = firestore.collection('users');
const productsCol =  firestore.collection('products');
import uuid from 'uuid-v4';
import path  from 'path';
import multer  from 'multer';

import * as Products from '../controllers/product';
import * as auth from '../middlewares/auth';

var bucket = admin.storage().bucket();

async function uploadFile(filename, name ,req) {
    let token = uuid()
    const metadata = {
        metadata: {
        // This line is very important. It's to create a download token.
        firebaseStorageDownloadTokens: token
        },
        contentType: 'image/png',
        cacheControl: 'public, max-age=31536000',
    };

  // Uploads a local file to the bucket
  await bucket.upload(filename, {
    // Support for HTTP requests made with `Accept-Encoding: gzip`
    gzip: true,
    metadata: metadata,
  }).then((data) => {
    let obj = {}
    obj.name = req.body.name;
    obj.stock = req.body.stock;
    obj.desc = req.body.desc;
    obj.price = req.body.price;
    obj.size = req.body.size;
    obj.gender = req.body.gender;
    obj.data = new Date();
    obj.category = req.body.category
    obj.img = "https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + name + "?alt=media&token=" + token
    productsCol.doc().set(obj)
 });

console.log(`${filename} uploaded.`);

}


const storage = multer.diskStorage({
    destination: "./uploads/",
    filename: function(req, file, cb){
        console.log(file)
        let name = "IMAGE-" + Date.now() + path.extname(file.originalname)
        cb(null, name);
        uploadFile(`../shop-back/uploads/${name}`, name, req, ).catch(console.error);
    }
 });
 
const upload = multer({
    storage: storage,
    limits:{fileSize: 10000000},
 })

const product = (app) => {
    app.post('/api/products/picture', upload.single('file'), (req, res) => {
        try {
            res.json({
                success: true
            });
        } catch(err) {
            res.send(400);
        }
    });
}

export default product;