import React, { useState, useEffect } from "react"
import { render } from "react-dom"
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Autocomplete from '@material-ui/lab/Autocomplete';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Routes from "../Routes";
import firebase from 'firebase/app';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import 'firebase/firestore'
const drawerWidth = 240;

let categories = [
  "Хувцас",
  "Бичиг хэрэг",
  "Гоо сайхан",
  "Гэр ахуй",
  "Ном",
  "Хүнс",
  "Эрүүл мэнд",
  "Бэлэг дурсгал"
]

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1
      },
      card: {
        maxWidth: '100%',
        marginBottom: 20
      },
      media: {
        height: 360
      },
      mediaMini: {
        height: 220
      },
      margin: {
        margin: theme.spacing(1),
        marginBottom: theme.spacing(5)
      },
      cardMini: {
        maxWidth: 360
      },
      extendedIcon: {
        marginRight: theme.spacing(1),
      },
      buttonCont: {
          display: 'flex',
          justifyContent: 'center'
      },
      paper: {
        position: 'absolute',
        width: 700,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
      modal: {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
      },
      form: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      button: {
          marginTop: 40
      },
      list: {
        width: 500
      }
  }));


function Orders(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(false);
    const [tournamentName, setTournamentName] = useState('');
    const [tournaments, setTournaments] = useState([]);
    const [rule, setRule] = useState('');
    const [isSingle, setIsSingle] = useState('');
    const [team, setTeam] = useState(16);
    const [fee, setFee] = useState(0);
    const [file, setFile] = useState({});
    const [data , setData]=useState([])
    const [category , setCategory]=useState("Хувцас")

    const getData = async () => {
      let products = await firebase.firestore().collection('orders').get()
      for(const item of products.docs) {
        let obj = item.data()
        obj.id = item.id
        setData(prevState => [
          ...prevState, obj
        ])
      }
    }

    useEffect(()=> {
      getData()
    }, [])  


    const deleteData = (id) => {
      setData([])
      getData()
      firebase.firestore().collection('orders').doc(id).delete()
    }

    console.log(data)

    console.log(tournaments)
    
    return (
        <div className="bg-purple-600">
                <Toolbar />  
                <Typography variant="h6" className={classes.title}>
            Захиалгууд
          </Typography>
          <div className={classes.demo}>
            <List>
                {
                  data.map((item,i) => 
                    <div>
                        <div className="ff">
                            Дугаар: {item.user.phone}
                        </div>
                        <div className="ff">
                            Хаяг: {item.address}
                        </div>
                        {
                            item.products.map((pro)=> 
                            <ListItem className={classes.list} key={i}>
                                <ListItemAvatar>
                                    <img style={{width: 50, height: 50, objectFit: 'cover'}} src={pro.img} />
                                </ListItemAvatar>
                                <ListItemText
                                    primary={pro.name}
                                    secondary={pro.price}
                                />
                                 <ListItemText
                                    primary={pro.quantity}
                                    secondary={pro.desc}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete">
                                    <DeleteIcon onClick={() => deleteData(item.id)} />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )
                        }
                    </div>
                  )
                }
            </List>
          </div>         
        </div>
    )
}

export default Orders;