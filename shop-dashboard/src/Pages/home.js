import React, { useState, useEffect } from "react"
import { render } from "react-dom"
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Autocomplete from '@material-ui/lab/Autocomplete';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Routes from "../Routes";
import firebase from 'firebase/app';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import 'firebase/firestore'
const drawerWidth = 240;

let categories = [
  "Хувцас",
  "Бичиг хэрэг",
  "Гоо сайхан",
  "Гэр ахуй",
  "Ном",
  "Хүнс",
  "Эрүүл мэнд",
  "Бэлэг дурсгал"
]

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1
      },
      card: {
        maxWidth: '100%',
        marginBottom: 20
      },
      media: {
        height: 360
      },
      mediaMini: {
        height: 220
      },
      margin: {
        margin: theme.spacing(1),
        marginBottom: theme.spacing(5)
      },
      cardMini: {
        maxWidth: 360
      },
      extendedIcon: {
        marginRight: theme.spacing(1),
      },
      buttonCont: {
          display: 'flex',
          justifyContent: 'center'
      },
      paper: {
        position: 'absolute',
        width: 700,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
      modal: {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
      },
      form: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      button: {
          marginTop: 40
      },
      list: {
        width: 500
      }
  }));


function Home(props) {
    const classes = useStyles();

    const [checked, setChecked] = useState([1]);
    const [openQualifier, setOpenQualifier] = React.useState({
        checked: false,
    });
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(false);
    const [tournamentName, setTournamentName] = useState('');
    const [size, setSize] = useState('');
    const [tournaments, setTournaments] = useState([]);
    const [rule, setRule] = useState('');
    const [isSingle, setIsSingle] = useState('');
    const [gender, setGender] = useState('Эм');
    const [team, setTeam] = useState(16);
    const [fee, setFee] = useState(0);
    const [file, setFile] = useState({});
    const [data , setData]=useState([])
    const [category , setCategory] = useState("Хувцас")

    const buttonClassname = clsx({
      [classes.buttonSuccess]: success,
    });
    console.log(gender)
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const getData = async () => {
      let products = await firebase.firestore().collection('products').get()
      for(const item of products.docs) {
        let obj = item.data()
        obj.id = item.id
        setData(prevState => [
          ...prevState, obj
        ])
      }
    }

    useEffect(()=> {
      getData()
    }, [])  

    const handleButtonClick = () => {
      if (!loading) {
        setSuccess(false);
        setLoading(true);
      }
    };

    const hasOpenQualifier = (event) => {
        setOpenQualifier({ ...openQualifier, [event.target.name]: event.target.checked });
    };

    const handleRadioChange = (event) => {
      setIsSingle(event.target.value);
    };

    const toInvite = (name, id) => {
      props.history.push({
        pathname: '/client/invite',
        state: { name: name, id: id }
      })
    }
    const deleteData = (id) => {
      setData([])
      getData()
      firebase.firestore().collection('products').doc(id).delete()
    }
    console.log(data)
    const create = () => {
      let formData = new FormData();
      formData.append("file", file);
      formData.append("name", tournamentName);
      formData.append("stock", team);
      formData.append("price", fee);
      formData.append("desc", rule);
      formData.append("category", category);
      formData.append("size", size);
      formData.append("gender", gender);
      let options = {
        method: "post",
        headers: {
          Accept: "application/json",
        },
        body: formData,
      };
      return fetch("http://localhost:5001/api/products/picture", options)
        .then((res) => {
          return res.json();
        })
        .then((json) => {
          console.log(json)
          alert("Амжилттай нэмэгдлээ")
        })
        .catch((err) => {});
    }

    const upload = (e) => {
      setFile(e[0])
    }

    console.log(tournaments)
    const modalBody = (
        <div className={classes.paper}>
          <div className={classes.form}>
            <div>
                <TextField
                  id="standard-full-width"
                  label="Барааны нэр"
                  style={{ margin: 8, marginBottom: 30 }}
                  placeholder="Барааны нэр"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                      shrink: true,
                  }}
                  onChange={(e)=> setTournamentName(e.target.value)}
                />
                <TextField
                label="Агуулах дахь тоо"
                id="margin-none"
                defaultValue={team}
                className={classes.textField}
                onChange={(e)=> setTeam(e.target.value)}
                />
                <FormControl>
                    <InputLabel htmlFor="standard-adornment-amount">Үнэ</InputLabel>
                    <Input
                        id="standard-adornment-amount"
                        value={fee}
                        startAdornment={<InputAdornment position="start">₮</InputAdornment>}
                        onChange={(e)=> setFee(e.target.value)}
                    />
                </FormControl>
                <Autocomplete
                  id="combo-box-demo"
                  options={categories}
                  getOptionLabel={(option) => option}
                  style={{ width: 300, marginTop: 30 }}
                  renderInput={(params) => <TextField {...params} label="Категори сонгох" variant="outlined" />}
                  onChange={(event, value) => setCategory(value)}
                />
                {
                  category === 'Хувцас' ? (
                    <div>
                    <div style={{marginBottom: 20}}>
                    <TextField
                    label="Size"
                    id="margin-none"
                    defaultValue={size}
                    className={classes.textField}
                    onChange={(e)=> setSize(e.target.value)}
                    />
                      </div>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Хүйс</FormLabel>
                      <RadioGroup aria-label="gender" name="gender1" onChange={e => setGender(e.target.value)}>
                        <FormControlLabel value="Эм" control={<Radio />} label="Эм" />
                        <FormControlLabel value="Эр" control={<Radio />} label="Эр" />
                      </RadioGroup>
                    </FormControl>
                    </div>
                  ):null
                }
                <TextField
                id="standard-full-width"
                label="Дэлгэрэнгүй мэдээлэл"
                style={{ margin: 8, marginTop: 30 }}
                placeholder="Дэлгэрэнгүй мэдээлэл"
                fullWidth
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={(e)=> setRule(e.target.value)}
                />
                {/* <FormControlLabel
                    style={{margin: 0, marginTop: 20}}
                    control={<Switch checked={openQualifier.checked} onChange={hasOpenQualifier} name="checked" />}
                    label="Open Qualifier-тай эсэх"
                    
                /> */}
                {/* <div style={{margin: 7, marginTop: 15}}>
                  <RadioGroup aria-label="quiz" name="quiz" value={isSingle} onChange={handleRadioChange}>
                    <FormControlLabel value="single" control={<Radio />} label="Single Elimination" />
                    <FormControlLabel value="double" control={<Radio />} label="Double Elimination" />
                  </RadioGroup>
                </div> */}
                <input style={{marginTop: 15, marginBottom: 15}} type="file" name="image" onChange={(e) => upload(e.target.files)}/>  Зураг оруулах

                <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Button className={classes.button} onClick={()=> create()} variant="outlined" color="secondary">Бараа оруулах</Button>
                </div>

            </div>
          </div>
        </div>
    );

    return (
        <div className="bg-purple-600">
                <Toolbar />
                <Modal
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    {modalBody}
                </Modal>
                <div className={classes.buttonCont}>
                    <Fab onClick={handleOpen} variant="extended" color="primary" aria-label="add" className={classes.margin}>
                        <AddIcon className={classes.extendedIcon} />
                        Бараа оруулах
                    </Fab>   
                </div>    
                <Typography variant="h6" className={classes.title}>
            Бүтээгдэхүүнүүд
          </Typography>
          <div className={classes.demo}>
            <List>
                {
                  data.map((item, i) => 
                    <Link to={{
                      pathname: 'single',
                      state: {
                        id: item.id
                      }
                    }}>
                      <ListItem className={classes.list} key={i}>
                        <ListItemAvatar>
                          <img style={{width: 50, height: 50, objectFit: 'cover'}} src={item.img} />
                        </ListItemAvatar>
                        <ListItemText
                          primary={item.name}
                          secondary={item.price}
                        />
                        <ListItemSecondaryAction>
                          <IconButton edge="end" aria-label="delete">
                            <DeleteIcon onClick={() => deleteData(item.id)} />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </Link>
                  )
                }
            </List>
          </div>         
        </div>
    )
}

export default Home;