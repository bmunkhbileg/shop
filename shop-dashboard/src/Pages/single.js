import React, { useState, useEffect } from "react"
import { render } from "react-dom"
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SendIcon from '@material-ui/icons/Send';
import DeleteIcon from '@material-ui/icons/Delete';
import firebase from 'firebase/app';
import 'firebase/firestore'
const drawerWidth = 240;
import './style.less'
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1
      },
  }));


function Single(props) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [comments, setComments] = useState([]);
    const [comment, setComment] = useState([]);
    const [data, setData] = useState({});

    const getData = async () => {
        let product = await firebase.firestore().collection('products').doc(props.location.state.id).get()
        setData(product.data())
    }

    console.log(data)

    useEffect(()=> {
        getData()
        firebase.firestore().collection('products').doc(props.location.state.id).collection('comments').limit(50).orderBy("date", "asc").onSnapshot((asd) => {
            asd.forEach((comment)=> {
                let obj = {}
                obj = comment.data()
                obj.id = comment.id
              setComments([...e, obj])
            })
          })
    }, [])  

    const sendComment = async () => {
        setComment('')
        setComments([])
        await firebase.firestore().collection('products').doc(props.location.state.id).collection('comments').add({
          date: new Date().getTime(),
          comment: comment, 
          user: 'Admin'
        })
      }
      const something = (e) => {
        if (e.keyCode === 13) {
          sendComment()
        }
      }
    const deleteComment = async (id) => {
        setComments([])
        await firebase.firestore().collection('products').doc(props.location.state.id).collection('comments').doc(id).delete()
    }

    
    return (
        <div className="bg-purple-600">
            <Toolbar />  
            <Typography variant="h6" className={classes.title}>
                {data.name}
            </Typography>
            <div className="single">
                <div className="image">
                <img src={data.img} />
                </div>
                <div className="right">
                    <div className="title">
                    {data.name}
                    </div>
                    <div className="desc">
                    {data.desc}
                    </div>
                </div>
            </div>
            <div className="commentCont">
                <div className="display">
                    {
                    comments === undefined || comments.length == 0 ? (
                        'Сэтгэгдэл байхгүй байна.'
                    ) : (
                        comments.map((item, i) =>
                        <div className="itemx">
                            <div className="name itemc">
                                {item.user}
                            </div>
                            <div className="comment itemc">
                                {item.comment}
                            </div>
                            <div className="date itemc">
                                {new Date(item.date).getHours()}:{new Date(item.date).getMinutes()} {new Date(item.date).getDate()}/{new Date(item.date).getMonth() + 1}/{new Date(item.date).getFullYear()}
                            </div>
                            <div className="delete">
                                <DeleteIcon onClick={()=> deleteComment(item.id)} />
                            </div>
                        </div>
                        )
                    )
                    }
                </div>
                <div className="send">
                    <input onKeyDown={(e) => something(e)} placeholder="Сэтгэгдэл үлдээх..." onChange={(e)=> setComment(e.target.value)} />
                     <SendIcon onClick={() => sendComment()} />
                </div>
            </div>
        </div>
    )
}

export default Single;