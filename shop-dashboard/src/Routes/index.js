import React, { Component } from "react";
import Home from "../Pages/home";
import Orders from "../Pages/Orders";
import Single from "../Pages/single";


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
function Routes() {
    return (      
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/orders" component={Orders} />
            <Route path="/single" component={Single} />
        </Switch>
    )
}
export default Routes;