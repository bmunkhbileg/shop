import React, { useEffect, useState } from "react"
import './firebase/config'
import { render } from "react-dom"
import Button from '@material-ui/core/Button';
import Main from './Pages'
import Login from './Pages/login'
import './style/style.less'
import CircularProgress from '@material-ui/core/CircularProgress'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
  withRouter
} from "react-router-dom";
import { createBrowserHistory } from 'history';


function App() {
  const [user, setUser] = useState(false);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log(user)
        firebase.firestore().collection('users').doc(user.uid).get()
          .then((data) => {
            setUser(true)
              setLoading(false)
          })
      } else {
        setUser(false)
        setLoading(false)
        console.log('no user')
      }
    });
  }, [])
    return (
      <>
      {
        loading ? (
          <div style={{color: 'black', display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%', position: 'fixed', width: '100%'}}><CircularProgress /></div>
        ) : (
          <div>
            
            {
              user ? (
                <div>
                  <Route exact path="/*" component={Main} />
                </div>
              ):(
                <div>
                  <Route exact path="/" component={Login} />
                  <Route path="/login" component={Login} />
                </div>
              )
            }
          </div>
        )
      }
    </>
    )
}

export default withRouter(App);

const wrapper = document.getElementById("root");
wrapper ? render(
<BrowserRouter>
  <App  history={createBrowserHistory}  />
</BrowserRouter>, 
wrapper) : false;