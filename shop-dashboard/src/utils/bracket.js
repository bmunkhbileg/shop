
  export const callBracket = async (count) => {

  let teams_array = [];
  
  //   let number_of_rounds = snapshot.data().total_rounds;
  let byeTeam = [], nonByeTeam = [];
  let participants = Array.from({length: count}, (v, k) => k + 1) ;
  let bracket = getBracket(participants);
  let smallest = smallestPow2(count); // get smallest power of 2
  let bye = smallest - count;         //number of teams not playing in round 1
  let nonBye = count - bye;           //round 2

  console.log(bye);
  console.log(nonBye)
  console.log(bracket)
  return bracket;
}

function getBracket(participants) {
  let participantsCount = participants.length;	
  let rounds = Math.ceil(Math.log(participantsCount)/Math.log(2));

  if(participantsCount < 2) {
    return [];
  }
    
  let matches = [[1,2]]

  for(let round = 1; round < rounds; round++) {
    let roundMatches = [];
    let sum = Math.pow(2, round + 1) + 1;
    for(let i = 0; i < matches.length; i++) {
      let home = changeIntoBye(matches[i][0], participantsCount);
      let away = changeIntoBye(sum - matches[i][0], participantsCount);
      roundMatches.push([home, away]);
      home = changeIntoBye(sum - matches[i][1], participantsCount);
      away = changeIntoBye(matches[i][1], participantsCount);
      roundMatches.push([home, away]);
    }
    matches = roundMatches;
  }   
  return matches;    
}

function changeIntoBye(seed, participantsCount) {
    return seed <= participantsCount ?  seed : null;
}



async function mergeArrays(teams_array, bracket, callback){
  //console.log(bracket)
  for(let i = 0; i < bracket.length; i++) {
    for(let j = 0; j < bracket[i].length; j++) {
      for(let x = 0; x < teams_array.length; x++) {
        if(bracket[i][j] == teams_array[x].number) {
          bracket[i][j] = {team_name : teams_array[x].team_name, number : teams_array[x].number }
        }
        if(bracket[i][j] == null) {
          bracket[i][j] = { team_name : null, number : null }
        }
      }
    }
  }
  callback(bracket);
}

function smallestPow2(n) {
  let count = 0;

  if(n > 0 && (n & (n -1)) == 0) {
      return n;
  }
  while(n != 0) {
      n >>= 1;
      count += 1;
  }

  return 1 << count;
}
