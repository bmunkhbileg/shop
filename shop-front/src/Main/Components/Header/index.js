import React, { Component, useState, useEffect } from "react";
import {
  IoIosSearch,
  IoIosNotifications,
  IoIosHelpCircle,
  IoIosCart,
  IoIosCloseCircleOutline,
  IoMdPerson 
} from "react-icons/io";
import "./header.less";
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import Ink from 'react-ink'
import onClickOutside from "react-onclickoutside";
const fs = firebase.firestore()

const clickOutsideConfig = {
  handleClickOutside: () => Header.handleClickOutside
};

 function Header(props) {
    const [user, setUser] = useState(false)
    const [userid, setUserid] = useState('')
    const [data1, setData] = useState([])
    const [modal, setModal] = useState(false)

    const handleClickOutside = evt => {
      // ..handling code goes here...
    };

    Header.handleClickOutside = () => {
      setModal(false)
    };

    const dataTrigger = async (uid) => {
      firebase.firestore().collection('Users').doc(uid).collection('cart').onSnapshot((snapshot) => {
        snapshot.docChanges().forEach((change) => {
            if (change.type === "added") {
                let obj = change.doc.data()
                obj.itemid = change.doc.id
                setData(prev => [...prev, obj])
            }
            if (change.type === "modified") {
                console.log("Modified city: ", change.doc.data());
            }
            if (change.type === "removed") {
                // setData(x)
            }
        });
    });
    }
    console.log(data1)
    useEffect(()=> {
      setData([])
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.
          console.log(user)
          setUser(user.email)
          setUserid(user.uid)
          dataTrigger(user.uid)
        } else {
          // No user is signed in.
        }
      });
      
    },[])
    const deleteData = async (id) => {
      let arr = data1;
      await firebase.firestore().collection('Users').doc(userid).collection('cart').doc(id).delete()
      let x = arr.filter(e => e.itemid !== id)
      setData(x)
    }
    const toCart = () => {
      setModal(false)
      props.history.push('/client/cart')
    }
    const logOut = () => {
      firebase.auth().signOut().then(() => {
        alert('Системээс амжилттай гарлаа')
        window.location.href = '/client'
      }).catch((error) => {
        // An error happened.
      });
    }
  return (
    <div className="header">
        <div className="search">
            <input placeholder="Хайх түлхүүр үгээ бичнэ үү..." />
            <IoIosSearch />
        </div>
        <div className="right">
            {/* <div className="item notif">
                Захиалга
                <IoIosNotifications />
            </div>
            <div className="item help">
              Тусламж
              <IoIosHelpCircle />
            </div> */}
            <div className="item cart">
              <span onClick={()=> modal ? setModal(false):setModal(true)}>Сагс</span>
              <IoIosCart onClick={()=> modal ? setModal(false):setModal(true)} />
              <div className="count">
                {data1.length}
              </div>
              {
                modal ? (
                  <div className="listCont">
                    {
                    data1 === undefined || data1.length == 0 ? (
                      "Сагс хоосон байна."
                    ):(
                      data1.map((item, i)=> 
                      <div onClick={()=> toCart()} key={i} className="listItem">
                        <Ink />
                        <div className="img">
                            <img src={item.img} />
                        </div>
                        <div className="title item">
                          {item.name}
                        </div>
                        <div className="quantity item">
                          {item.quantity}
                        </div>
                        <div className="price item">
                          {item.price}₮
                        </div>
                        <div onClick={()=> deleteData(item.itemid)} className="x item">
                          <IoIosCloseCircleOutline />
                          <Ink />
                        </div>
                      </div>
                    )
                    )
                    }
                  </div>
                ):null
              }
            </div>
            {
              !user ? (
                <div className="item login">
                  <a href="/client/login">Нэвтрэх</a>
                  <IoMdPerson />
                </div>
              ):(
                <div className="item login">
                  {user}
                  <IoMdPerson />
                  <div onClick={()=> logOut()} className="logout">
                    Гарах
                    <Ink />
                  </div>
                </div>
              )
            }
        </div>
    </div>
  );
}


export default onClickOutside(Header, clickOutsideConfig)
