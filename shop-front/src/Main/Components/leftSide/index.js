import React, { Component,useEffect,useState } from "react";
import "./leftside.less";
import {
  IoMdShirt,
  IoMdBook,
  IoIosWoman,
  IoIosHome,
  IoIosBrush,
  IoMdThermometer,
  IoIosGift,
  IoLogoApple
} from "react-icons/io";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { GiSwordsEmblem } from "react-icons/gi";
function LeftSide(props) {
  const [index,setIndex]=useState(0)
  useEffect(()=>{
      let pageURL = window.location.href;
      var values = pageURL.split(/[//]+/);
      setIndex(values[3]);
      checkIndex(values[3])
  })
  const checkIndex = (item) => {
    switch(item){
      case "": setIndex(0)
      break;
      case "profile": setIndex(1)
      break;
      case "wallet": setIndex(2)
      break;
      case "tlist": setIndex(3)
      break;
      case "tprofile": setIndex(3)
      break;
      case "lan": setIndex(4)
      break;
      case "league": setIndex(5)
      break;
      case "rank": setIndex(6)
      break;
      case "organizers": setIndex(7)
      break;
      case "teams": setIndex(8)
      break;
      case "teampro": setIndex(8)
      break;
      case "cteam": setIndex(8)
      break;
      default: setIndex(0)
    }
  }
  return (
    <div className="leftside">
      <Link to="/client" className="logoCont">
        <div style={{color: '#fff', fontWeight: 'bold', fontSize: 25}}>Tetse.mn</div>
      </Link>
      <div className="list">
        <div className="flex">
          <Link
          to={{
            pathname: '/client/categories',
            state: {
              category: 'Хувцас'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoMdShirt />
              </div>
              <div className="titleList">Хувцас</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Бичиг хэрэг'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoIosBrush />
              </div>
              <div className="titleList">Бичиг хэрэг</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Гоо сайхан'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoIosWoman />
              </div>
              <div className="titleList">Гоо сайхан</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Гэр ахуй'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoIosHome />
              </div>
              <div className="titleList">Гэр ахуй</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Ном'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoMdBook />
              </div>
              <div className="titleList">Ном</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Хүнс'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoLogoApple />
              </div>
              <div className="titleList">Хүнс</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Эрүүл мэнд'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoMdThermometer />
              </div>
              <div className="titleList">Эрүүл мэнд</div>
          </Link>
          <Link to={{
            pathname: '/client/categories',
            state: {
              category: 'Бэлэг дурсгал'
            }
          }} className={`topList ${index==3?"active":null}`}>
              <div className="listIcon">
                <IoIosGift />
              </div>
              <div className="titleList">Бэлэг дурсгал</div>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default LeftSide;
