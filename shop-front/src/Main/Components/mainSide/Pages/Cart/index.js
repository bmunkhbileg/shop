import React, { Component,useState,useEffect } from "react";
import "./cart.less";
import { IoMdArrowRoundForward } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase, { firestore } from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import Ink from 'react-ink'

import { IoTrophy } from "react-icons/io5";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Cart(props) {
  
  const [user, setUser] = useState(null)
  const [userInfo, setUserInfo] = useState({})
  const [userid, setUserid] = useState(null)
  const [data, setData] = useState([])
  const [all, setAll] = useState(0)
  const [address, setAddress] = useState("")

  const dataTrigger = async (uid) => {
    let items = await firebase.firestore().collection('Users').doc(uid).collection('cart').get();
    for(let item of items.docs) {
        let obj = item.data()
                obj.itemid = item.id
        setData(e => [...e, obj])
        setAll(e => e + Number(item.data().price))
    }
  }
  console.log(data)
  useEffect(()=> {
    setData([])
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        console.log(user)
        setUser(user.email)
        setUserid(user.uid)
        dataTrigger(user.uid)
        firebase.firestore().collection('Users').doc(user.uid).get()
        .then((res) => {
            setUserInfo(res.data())
            setAddress(res.data().address)
        })
      } else {
        // No user is signed in.
      }
    });
  }, [props])  
  const deleteData = async (id) => {
    let arr = data;
    await firebase.firestore().collection('Users').doc(userid).collection('cart').doc(id).delete()
    let x = arr.filter(e => e.itemid !== id)
    setData(x)
  }
  const order = async () => {
    await firebase.firestore().collection('orders').add({
        address: address,
        products: data,
        user: userInfo
    })
    let products = await firebase.firestore().collection('Users').doc(userid).collection('cart').get()
    for(let product of products.docs) {
        await firebase.firestore().collection('Users').doc(userid).collection('cart').doc(product.id).delete()
    }
    window.location.href = '/client'
    alert('Таны захиалгыг хүлээн авлаа. Төлбөр төлбөгдсөн үед баталгаажина. Баярлалаа')
    
  }
  return (
    <div className="cartx">
        <div className="left">
            <div className="item">
                <div className="title">
                    <span>1</span>
                    <span>Сагс</span>
                </div>
                {
                    data.map((item, i) =>
                        <div key={i} className="content">
                            <img src={item.img} />
                            <div className="c">
                                Үнэ: {item.price}₮   
                            </div>
                            <div className="c">
                                Тоо ширхэг: {item.quantity}
                            </div>
                            <button onClick={()=> deleteData(item.itemid)}>
                                    Устгах
                                    <Ink />
                            </button>
                        </div>
                    )
                }
                <div className="all">
                   Нийт үнэ: {all}₮
                </div>
            </div>
            <div className="item">
                <div className="title">
                    <span>2</span>
                    <span>Төлбөр төлөх</span>
                </div>
                <div className="content gg">
                    Төлбөрийг Хаан банкны 5500550055 дансанд шилжүүлнэ үү. Гүйлгээний утга: {user}
                </div>
            </div>
            <div className="item">
                <div className="title">
                    <span>3</span>
                    <span>Хүргэлт</span>
                </div>
                <div className="content gg xx">
                    Доорх хаягаас өөр хаяг оруулах бол доор бичнэ үү.
                    <input value={address} placeholder={userInfo.address} onChange={e => setAddress(e.target.value)} />
                </div>
            </div>
        </div>
        <div className="right">
            <div className="item">
                <div className="title">
                    <span>3</span>
                    <span>Баталгаажуулалт</span>
                </div>
                <button onClick={()=> order()}>
                    Захиалга өгөх
                    <Ink />
                </button>
            </div>
        </div>
    </div>
  );
}

export default Cart;
