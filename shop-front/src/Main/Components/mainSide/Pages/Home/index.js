import React, { Component,useState,useEffect } from "react";
import "./home.less";
import { IoMdArrowRoundForward } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { IoTrophy } from "react-icons/io5";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Home(props) {
  const [count,setCount]=useState(0)
  const [data , setData]=useState([])
  const [loading,setLoading] = useState(true)

  const getData = async () => {
    let products = await firebase.firestore().collection('products').get()
    for(const item of products.docs) {
      let obj = item.data()
      obj.id = item.id
      setData(prevState => [
        ...prevState, obj
      ])
    }
  }

  useEffect(()=> {
      getData()
  }, [])  
  console.log(data)
  return (
    <div className="shopitemscont">
        
        {
          data.map((item, i) => 
          <Link
          to={{
            pathname: '/client/single',
            state: {
              id: item.id
            }
          }} className={`topList`}>
              <div key={i} className="item">
                <div className="image">
                    <img src={item.img} />
                </div>
                <div className="title">
                  {item.name}
                </div>
                <div className="price">
                  {item.price}₮
                </div>
            </div>
          </Link>
            
          )
        }
    </div>
  );
}

export default Home;
