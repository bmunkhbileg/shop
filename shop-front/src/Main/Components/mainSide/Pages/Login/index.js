import React, { Component,useState,useEffect } from "react";
import "./home.less";
import { IoMdArrowRoundForward } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { IoTrophy } from "react-icons/io5";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Login(props) {
  const [count1, setCount1] = useState(0)
  const [data , setData] = useState({})
  const [loading,setLoading] = useState(true)
  const [email,setEmail] = useState("")
  const [pass,setPass] = useState("")

  const register = () => {
    firebase.auth().signInWithEmailAndPassword(email, pass)
    .then((userCredential) => {
      // Signed in
      var user = userCredential.user;
      alert('Амжилттай нэвтэрлээ')
      window.location.href = "/client"
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(error)
      alert('Нэвтрэх нэр эсвэл нууц үг буруу байна.')
    });
  }

  return (
    <div className="login sd">
        <input placeholder="E-mail" onChange={e => setEmail(e.target.value)} />
        <input type="password" placeholder="нууц үг" onChange={e => setPass(e.target.value)}/>
        <button onClick={()=> register()}>Нэвтрэх</button>
        <div />
        <a href="/client/register"> Эсвэл бүртгүүлэх </a>
    </div>
  );
}

export default Login;
