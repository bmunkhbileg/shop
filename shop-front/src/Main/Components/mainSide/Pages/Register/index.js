import React, { Component,useState,useEffect } from "react";
import "./home.less";
import { IoMdArrowRoundForward } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { IoTrophy } from "react-icons/io5";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Register(props) {
  const [email,setEmail] = useState("")
  const [address,setAddress] = useState("")
  const [phone,setPhone] = useState("")
  const [pass,setPass] = useState("")

  const register = () => {
    firebase.auth().createUserWithEmailAndPassword(email, pass)
    .then((userCredential) => {
      // Signed in 
      var user = userCredential.user;
      firebase.firestore().collection('Users').doc(user.uid).set({
        uid: user.uid,
        email: email,
        address: address, phone: phone
      })
      console.log(user)
      alert('Бүртгэл амжилттай үүслээ')
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(error)
      // ..
    });
  }

  return (
    <div className="login sd">
        <input placeholder="E-mail" onChange={e => setEmail(e.target.value)} />
        <input type="password" placeholder="нууц үг" onChange={e => setPass(e.target.value)}/>
        <input placeholder="Гар утасны дугаар" onChange={e => setPhone(e.target.value)} />
        <input placeholder="Гэрийн хаяг" onChange={e => setAddress(e.target.value)}/>
        <button onClick={()=> register()}>Бүртгүүлэх</button>
    </div>
  );
}

export default Register;
