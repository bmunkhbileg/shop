import React, { Component,useState,useEffect } from "react";
import "./home.less";
import { IoMdArrowRoundForward, IoMdSend } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Single(props) {
  const [count1, setCount1] = useState(1)
  const [data , setData] = useState({})
  const [loading,setLoading] = useState(true)
  const [user,setUser] = useState(null)
  const [comment, setComment] = useState('')
  const [comments, setComments] = useState([])
  const [userEmail, setUserEmail] = useState('')

  const getData = async () => {
    console.log(props.location.state.id)
    let product = await firebase.firestore().collection('products').doc(props.location.state.id).get()
    firebase.firestore().collection('products').doc(props.location.state.id).collection('comments').limit(50).orderBy("date", "asc").onSnapshot((asd) => {
      asd.forEach((comment)=> {
        setComments(e => [...e, comment.data()])
      })
    })
    setData(product.data())
  }
  console.log(comments)
  useEffect(()=> {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        console.log(user)
        setUser(user.uid)
        setUserEmail(user.email)
      } else {
        // No user is signed in.
      }
    });
      getData()
  }, [props])  

  const addCart = async () => {
    if(user == null) {
      props.history.push('/client/login')
    } else {
      let obj = data;
    obj.quantity = count1;
    obj.id = props.location.state.id;
    await firebase.firestore().collection('Users').doc(user).collection('cart').add(obj)
    }
  }

  const sendComment = async () => {
    setComment('')
    setComments([])
    await firebase.firestore().collection('products').doc(props.location.state.id).collection('comments').add({
      date: new Date().getTime(),
      comment: comment, 
      user: user ? userEmail : 'Зочин'
    })
  }

  const something = (e) => {
    if (e.keyCode === 13) {
      sendComment()
    }
  }
  return (
    <>
      <div className="single">
        <div className="image">
          <img src={data.img} />
        </div>
        <div className="right">
            <div className="title">
              {data.name}
            </div>
            <div className="desc">
              {data.desc}
            </div>
            <div className="info">
              <span>{data.price}₮</span>
              {
                data.category === 'Хувцас' ? (
                  <>
                  <span>Хэмжээ: {data.size}</span>
                  <span>Хүйс: {data.gender}</span>
                  </>
                ):null
              }
              <span>Хямдрал: 0₮</span>
              <span>Хэмнэлт: 0₮</span>
            </div>
            <div className="count">
              <span onClick={() => count1 > 0 ? setCount1(prevCount => prevCount - 1):null}>-</span>
              <span>{count1}</span>
              <span onClick={() => setCount1(prevCount => prevCount + 1)}>+</span>
            </div>
            <button onClick={()=> addCart()}>Сагсанд нэмэх</button>
        </div>
      </div>
      <div className="commentCont">
        <div className="display">
            {
              comments === undefined || comments.length == 0 ? (
                'Сэтгэгдэл байхгүй байна.'
              ) : (
                comments.map((item, i) =>
                  <div className="itemx">
                      <div className="name itemc">
                          {item.user}
                      </div>
                      <div className="comment itemc">
                          {item.comment}
                      </div>
                      <div className="date itemc">
                          {new Date(item.date).getHours()}:{new Date(item.date).getMinutes()} {new Date(item.date).getDate()}/{new Date(item.date).getMonth() + 1}/{new Date(item.date).getFullYear()}
                      </div>
                  </div>
                )
              )
            }
        </div>
        <div className="send">
            <input onKeyDown={(e) => something(e)} placeholder="Сэтгэгдэл үлдээх..." onChange={(e)=> setComment(e.target.value)} />
            <IoMdSend onClick={() => sendComment()} />
        </div>
      </div>
    </>
  );
}

export default Single;
