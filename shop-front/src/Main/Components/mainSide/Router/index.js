import React, { Component } from "react";
import Home from "../Pages/Home";
import Categories from "../Pages/Categories";
import Single from "../Pages/Single";
import Login from "../Pages/Login";
import Register from "../Pages/Register";
import Cart from "../Pages/Cart";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function Routes(props) {
  return (
    <Switch>
        <Route exact path="/client"  component={Home} />
        <Route path="/client/categories"  component={Categories} />
        <Route path="/client/single"  component={Single} />
        <Route path="/client/login"  component={Login} />
        <Route path="/client/register"  component={Register} />
        <Route path="/client/cart"  component={Cart} />
    </Switch>
  );
}
export default Routes;
