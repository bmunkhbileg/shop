import React, { Component } from "react";
import { IoMdArrowRoundForward } from "react-icons/io";
import Routes from './Router'
function MainSide() {
  return (
    <div className="mainside">
      <Routes />
    </div>
  );
}

export default MainSide;
