import React, { Component, useEffect, useState } from "react";
import "./login.less";
import * as authActions from '../../../api/actions/authActions'
import { Spinner } from 'react-activity';
import firebase from 'firebase/app';
import 'firebase/auth';
import { useAlert } from 'react-alert'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

let fbauth = firebase.auth();

function Login(props) {

  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const alert = useAlert()

  const auth = (user) => {
        firebase.auth().signInWithCustomToken(user.firebaseCustomToken)
            .then((res)=>{
                localStorage.setItem('User', JSON.stringify(user));
                firebase.firestore()
                        .collection('Champ_Users')
                        .doc(user.username)
                        .get()
                        .then((res)=>  {
                          setLoading(false)
                            if(res.data().steam64 === null || res.data().steam64 === undefined) {
                                window.location.href = `https://champs.camp/steam/check/${user.username}`
                            } else {
                                props.history.push('/client')
                            }
                  })
            })
            .catch(function(error) {
                console.log(error.code,error.message)
            });
  }

  const login = () => {
    setLoading(true);
    console.log(authActions.userLogin())
    authActions.userLogin({ email: email, password: password })
    .then((res) => {
      console.log(res)
      switch (res.status) {
            case 400:
                alert.info('Мэдээллээ бүрэн оруулна уу');
                setLoading(false);
                break;
            case 422:
                alert.info('Хэрэглэгч олдсонгүй');
                setLoading(false);
                break;
            case 404:
                alert.info('Хэрэглэгчийн нэр эсвэл нууц үг буруу байна');
                setLoading(false);
                break;
            case 401:
                alert.info('Хэрэглэгчийн нэр эсвэл нууц үг буруу байна');
                setLoading(false);
                break;
            case 200:
                let user = res.payload;
                auth(user);
                break;
        }
    })
  }

  const handleKeypress = (e) => {
    if (e.key === 'Enter') {
      login();
    }
  };

  return (
    <div className="Login">
      <div className="logoCont">
          <img className="logo" src="/assets/images/mainlogo.png" />
      </div>
      <div
        className="bg"
        style={{
          backgroundImage: `url('/assets/images/register_player_bg.png')`,
        }}
      >
        <img className="layer" src="/assets/images/register_player.png" />
      </div>
      <div className="inputCont">
        <div className="ititle">Нэвтрэх</div>
        <div className="desc">Эргээд тавтай морилно уу!</div>
        <input placeholder="E-mail хаяг" onChange={(e)=> setEmail(e.target.value)} onKeyPress={handleKeypress} />
        <input placeholder="Нууц үг" onChange={(e)=> setPassword(e.target.value)} type="password" onKeyPress={(e)=> handleKeypress(e)} />
        
        <Link to={"/forgot"}><div className="forgot">Нууц үг мартсан?</div></Link> 
        <div className="buttonCont">
          <button onClick={()=> login()} >
            Нэвтрэх 
            {
              loading ? (<Spinner color="#000" speed={0.8} />):null
            }
          </button>
          <Link to={"/register"}>  <button>Бүртгүүлэх</button></Link> 
        </div>
      </div>
    </div>
  );
}

export default Login;
