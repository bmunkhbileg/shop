import React, { Component, useState } from "react";
import "./register.less";
import { useAlert } from 'react-alert'
import * as authActions from '../../../api/actions/authActions'
import { Spinner } from 'react-activity';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function Register() {
  const alert = useAlert()
  const [bool, setBool] = useState(false);
  const [boolP, setBoolP] = useState(false);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [phone, setPhone] = useState("");
  const [verify, setVerify] = useState(false);
  const [pin, setPin] = useState("");

  function checkEmail(e) {
    setEmail(e);
    var re = /\S+@\S+\.\S+/;
    if (re.test(e)) {
      setBool(true);
    } else {
      setBool(false);
    }
  }

  const verifyPin = () => {
    setLoading(true)
    authActions.verifyRegister({
      username:username,
      email:email,
      password:password,
      password2:confirmPassword,
      phone:phone,
      profile_picture:'',
      verification_code:pin,
      ref:''
  })
      .then((res)=>{
        console.log(res)
          switch (res.status) {
            case 410:
              console.log(res)
              break; 
              case 200:
                alert.info('Амжилттай бүртгэгдлээ та нэвтрэнэ үү.');
                setTimeout(()=> {
                  setLoading(false)
                  window.location.href = "/login"
                },1000)
               
                  break;
              case 400:
                alert.info('Нууц үг таарахгүй байна');
                setLoading(false)
                  break;
          }
          
      })
      .catch((err)=>{
          console.log(err)
      })
  }
  const register = () => {
    console.log(username, phone)
    setLoading(true)
    if(password === confirmPassword) {
      if(password.length < 6) {
        alert.info('Нууц үг 6-аас дээш оронтой байх ёстой.');
          setLoading(false)
      } else {
          authActions.createUser({
              username:username,
              email:email,
              password:password,
              phone: phone,
              confirm_pass: confirmPassword
          })
              .then((res)=>{
                  console.log(res)
                  
                  switch (res.status) {
                      case 200:
                          if(res.payload.username) {
                            alert.info('Хэрэглэгчийн нэр 4 өөс 10 оронтой тусгай тэмдэгт оролцоогүй байх ёстойг анхаарна уу');
                          } else if(res.payload.email) {
                            alert.info('Хэрэглэгчийн e-mail буруу байна.');
                          } else {
                            setVerify(true)
                          setLoading(false)
                          }
                          break;
                      case 400:
                          if(res.payload.errorMessage === "Phone number is already in use.") {
                            alert.info('Бүртгэлгүй дугаар ашиглана уу.');
                              setLoading(false)
                          } else if(res.payload.errorMessage === "Username is already in use.") {
                            alert.info('Хэрэглэгчийн нэр бүртгэлтэй байна.');
                              setLoading(false)
                          } else if(res.payload.errorMessage === "Email is already in use.") {
                            alert.info('Хэрэглэгчийн e-mail бүртгэлтэй байна.');
                              setLoading(false)
                          } else {
                            alert.info('Мэдээллээ бүрэн оруулна уу');
                              setLoading(false)
                          }
                          break;
                      case 422:
                          if(res.payload[0].param === 'password') {
                            alert.info('Нууц үг дор хаяж нэг том үсэг, нэг тоо байна.');
                              setLoading(false)
                          } else if(res.payload[0].param === 'username'){
                            alert.info('Хэрэглэгчийн нэр 4 буюу түүнээс дээш оронтой тусгай тэмдэгт оролцоогүй байх ёстойг анхаарна уу');
                              setLoading(false)
                          }
                          else if(res.payload[0].param === 'phone'){
                            alert.info('Утасны дугаараа зөв оруулна уу');
                              setLoading(false)
                          } else if(res.payload[0].param === 'email'){
                            alert.info('E-mail хаягаа зөв оруулна уу');
                              setLoading(false)
                          } else {
                            alert.info('Мэдээллээ бүрэн зөв бөглөнө үү');
                              setLoading(false)
                          }
                          break;
                      case 404:
                        alert.info('Серверт алдаа гарлаа.');
                          setLoading(false)
                          break;
                      case 429:
                        alert.info('Нэг IP хаягнаас олон хандалт ирсэн тул түр хүлээгээд дахин оролдоно уу');
                          setLoading(false)
                          break;
                  }
                  setLoading(false)
              })
              .catch((err)=>{
                  console.log(err)
              })
      }
  } else {
    alert.info('Нууц үг таарахгүй байна');
      setLoading(false)
  }
  }
  const handleKeypress2 = (e) => {
    if (e.key === 'Enter') {
      verifyPin();
    }
  };
  const handleKeypress = (e) => {
    if (e.key === 'Enter') {
      register();
    }
  };
  return (
    <div className="register">
      <div className="logoCont">
        <img className="logo" src="/assets/images/mainlogo.png" />
      </div>
      <div
        className="bg"
        style={{
          backgroundImage: `url('/assets/images/register_player_bg.png')`,
        }}
      >
        <img className="layer" src="/assets/images/register_player.png" />
      </div>
      {
        verify ? (
          <div className="inputCont">
            <div className="ititle">Баталгаажуулах</div>
            <div className="desc">E-mail хаяганд ирсэн 4 оронтой тоогоор баталгаажуулна уу</div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="E-mail хаяганд ирсэн баталгаажуулах код"
                  value={pin}
                  onChange={(e) => setPin(e.target.value)}
                  onKeyPress={(e)=> handleKeypress2(e)} 
                />
                
              </div>
            
            </div>
            
            <div className="buttonCont">
              <button onClick={()=> verifyPin()}> Баталгаажуулах 
              {
              loading ? (<Spinner color="#000" speed={0.8} size={12} />):null
            }
              </button>
           
            </div>
          </div>
        ):(
          <div className="inputCont">
            <div className="ititle">Бүртгүүлэх</div>
            <div className="desc">Аялалаа эхлүүл!</div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="Нэвтрэх нэр"
                  onChange={(e) => setUsername(e.target.value)}
                  onKeyPress={(e)=> handleKeypress(e)} 
                />
                
              </div>
              {username == "" ? (
                <span>Нэвтрэх нэрээ оруулна уу!</span>
              ) : (
                <span></span>
              )}
            </div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="И-Мэйл"
                  onChange={(e) => checkEmail(e.target.value)}
                  onKeyPress={(e)=> handleKeypress(e)} 
                />
                {email == "" ? null : (
                  <div className={bool ? "icon" : "iconClose"}>
                  </div>
                )}
              </div>
              {email == "" ? (
                <span>И-Мэйл хаяг аа оруулна уу!</span>
              ) : !bool ? (
                <span>И-Мэйл хаяг буруу байна</span>
              ) : (
                <span></span>
              )}
            </div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="Утасны дугаар"
                  onChange={(e) => setPhone(e.target.value)}
                  onKeyPress={(e)=> handleKeypress(e)} 
                />
                {email == "" ? null : (
                  <div className={bool ? "icon" : "iconClose"}>
                  </div>
                )}
              </div>
            </div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="Нууц үг"
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                  onKeyPress={(e)=> handleKeypress(e)} 
                />
              </div>

            </div>
            <div className="inputSection">
              <div className="input">
                <input
                  placeholder="Нууц үг давт"
                  type="password"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                  onKeyPress={(e)=> handleKeypress(e)} 
                />
                
              </div>
              {confirmPassword === "" ? (
                <span>Нууц үгээ баталгаажуулна уу</span>
              ) : password !== confirmPassword ? (
                <span>Нууц үг таарахгүй байна</span>
              ) : (
                <span></span>
              )}
            </div>
            <div className="buttonCont">
              <button onClick={()=> register()}> Бүртгүүлэх
              {
              loading ? (<Spinner color="#000" speed={0.8} size={12} />):null
            }
              </button>
              <div className="already">
              <Link to={"/login"}> <span className="span">Аль хэдийн бүртгүүлсэн</span> </Link>                
              </div>
            </div>
          </div>
        )
      }
    </div>
  );
}

export default Register;
