import React, { Component, useState, useEffect } from "react";
import Header from './Components/Header'
import LeftSide from './Components/leftSide'
import MainSide from './Components/mainSide'

import Routes from './Components/mainSide/Router'
import { Spinner } from 'react-activity';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

function Main(props) {

    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        setLoading(false)
        
    }, [])
    console.log(props)
    return (
        <div>
            {
                loading ? (
                    <div className="loader">
                        <Spinner size={25} />
                    </div>
                ):(
                    <div>
                        <Header {...props} />
                        <LeftSide {...props} />
                        <MainSide {...props} />
                    </div>
                )
            }
        </div>
    )
}

export default Main;