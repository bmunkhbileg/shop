import React, { Component } from "react";
import Main from "../Main";
import Login from "../Main/Pages/Login";
import Register from "../Main/Pages/Register";
import PrivateRoute from "../PrivateRoute"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
function Routes() {
    return (      
        <Switch>
            <PrivateRoute path="/login*" component={Login} />
            <PrivateRoute exact path="/c*" component={Main} />
            <PrivateRoute path="/register" component={Register} />
        </Switch>
    )
}
export default Routes;