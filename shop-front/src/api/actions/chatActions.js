import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const saveDrMsg = async (data) => {
    return networkHandler(
        urls.saveDrMsg,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const saveTeamMsg = async (data) => {
    return networkHandler(
        urls.saveTeamMsg,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const savePublicMsg = async (data) => {
    return networkHandler(
        urls.savePublicMsg,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const sendLanChat = async (data) => {
    return networkHandler(
        urls.sendLanChat,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};


export const sendTournamentChat = async (data) => {
    return networkHandler(
        urls.sendTournamentChat,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
