import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const sendRequest = async (data) => {
    return networkHandler(
        urls.sendRequest,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const acceptRequest = async (data) => {
    return networkHandler(
        urls.acceptRequest,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const declineRequest = async (data) => {
    return networkHandler(
        urls.declineRequest,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const removeFriend = async (data) => {
    return networkHandler(
        urls.removeFriend,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const cancelFriend = async (data) => {
    return networkHandler(
        urls.cancelFriend,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};


