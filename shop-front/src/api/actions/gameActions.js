import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";
import * as constants from "../constants";


export const changeMode = (data)=> {
    return {
        type: "GAME_MODE",
        data
    }
}

export const findingMatch = (data)=> {
    return {
        type: "FINDING_MATCH",
        data
    }
};

export const findMatchSolo = async (data) => {
    return networkHandler(
        urls.findMatchSolo,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const findMatchParty = async (data) => {
    return networkHandler(
        urls.findMatchParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const findCsGo = async (data) => {
    return networkHandler(
        urls.findCsGo,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const declineMatch = async (data) => {
    return networkHandler(
        urls.declineMatch,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const acceptMatch = async (data) => {
    return networkHandler(
        urls.acceptMatch,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const changeGame = async (data) => {
    console.log(data)
    return networkHandler(
        urls.changeGame,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};