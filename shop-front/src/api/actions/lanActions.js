import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";
import * as constants from "../constants";

export const createLan = async (data) => {
    return networkHandler(
        urls.createLan,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const getLanList = async (data) => {
    return networkHandler(
        urls.lanPageFirst,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const joinLan = async (data) => {
    return networkHandler(
        urls.lanJoin,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};


export const changeSlot = async (data) => {
    return networkHandler(
        urls.changeLan,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const lanBet = async (data) => {
    return networkHandler(
        urls.lanBet,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const lanBetSettings = async (data) => {
    return networkHandler(
        urls.lanBetSettings,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};


export const lanReady = async (data) => {
    return networkHandler(
        urls.lanReady,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const lanReadyCancel = async (data) => {
    return networkHandler(
        urls.lanReadyCancel,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const lanLeave = async (data) => {
    return networkHandler(
        urls.lanLeave,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const lanStart = async (data) => {
    return networkHandler(
        urls.lanStart,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};


export const kickLan = async (data) => {
    return networkHandler(
        urls.kickLan,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
