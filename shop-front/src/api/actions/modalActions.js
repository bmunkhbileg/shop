import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";
import * as constants from "../constants";


export const openModal = (data)=> {
    return {
        type: "OPEN_MODAL",
        data
    }
}
