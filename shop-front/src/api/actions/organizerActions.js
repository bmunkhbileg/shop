import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const getOrganizer = async (data) => {
    return networkHandler(
        urls.getOrganizer,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
