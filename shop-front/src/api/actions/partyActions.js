import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";


export const sendParty = async (data) => {
    return networkHandler(
        urls.sendParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const acceptParty = async (data) => {
    return networkHandler(
        urls.acceptParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const declineParty = async (data) => {
    return networkHandler(
        urls.declineParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const cancelParty = async (data) => {
    return networkHandler(
        urls.cancelParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const kickParty = async (data) => {
    return networkHandler(
        urls.kickParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const leaveParty = async (data) => {
    return networkHandler(
        urls.leaveParty,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
