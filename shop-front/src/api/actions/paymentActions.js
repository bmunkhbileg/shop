import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";
import * as constants from "../constants";

export const generateQr = async (data) => {
    return networkHandler(
        urls.generateQr,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const checkAmount = async (data) => {
    return networkHandler(
        urls.checkAmount,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const withdraw = async (data) => {
    return networkHandler(
        urls.withdraw,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const set_bankaccount = async (data) => {
    return networkHandler(
        urls.set_bankaccount,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const verify_bankaccount = async (data) => {
    return networkHandler(
        urls.verify_bankaccount,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const bank_req = async (data) => {
    return networkHandler(
        urls.bank_req,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const bank_req_done = async (data) => {
    return networkHandler(
        urls.bank_req_done,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const getPremium = async (data) => {
    return networkHandler(
        urls.getPremium,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const cp = async (data) => {
    return networkHandler(
        urls.cp,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
