import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const getRanking = async (data) => {
    return networkHandler(
        urls.getRanking,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const getRankingNext = async (data) => {
    return networkHandler(
        urls.getRankingNext,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};





