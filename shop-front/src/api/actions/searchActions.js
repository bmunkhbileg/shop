import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const search = async (data) => {
    return networkHandler(
        urls.search,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
