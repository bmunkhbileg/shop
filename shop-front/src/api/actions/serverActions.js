import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const MatchJoin = async (data) => {
  return networkHandler(urls.MatchJoin, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const leavaMatch = async (data) => {
  return networkHandler(urls.leavaMatch, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const joinSideMatch = async (data) => {
  return networkHandler(urls.joinSideMatch, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const sendChat = async (data) => {
  return networkHandler(urls.sendChatMatch, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const readyMatch = async (data) => {
  return networkHandler(urls.readyMatch, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const unreadyMatch = async (data) => {
  return networkHandler(urls.unreadyMatch, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
