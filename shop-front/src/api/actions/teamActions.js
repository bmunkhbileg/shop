import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const createteam = async (data) => {
    return networkHandler(
        urls.createTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const getTeam = async (data) => {
    return networkHandler(
        urls.getTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const getTeamNext = async (data) => {
    return networkHandler(
        urls.getTeamNext,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const getTeamPrev = async (data) => {
    return networkHandler(
        urls.getTeamPrev,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const getTeamProfile = async (data) => {
    return networkHandler(
        urls.getTeamProfile,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const searchMember = async (data) => {
    return networkHandler(
        urls.searchMember,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const inviteTeam = async (data) => {
    return networkHandler(
        urls.inviteTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const cancelTeam = async (data) => {
    return networkHandler(
        urls.cancelTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const declineTeam = async (data) => {
    return networkHandler(
        urls.declineTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const deleteTeam = async (data) => {
    return networkHandler(
        urls.deleteTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const acceptTeam = async (data) => {
    return networkHandler(
        urls.acceptTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const kickMember = async (data) => {
    return networkHandler(
        urls.kickMember,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const leaveTeam = async (data) => {
    return networkHandler(
        urls.leaveTeam,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const changeTeamName = async (data) => {
    return networkHandler(
        urls.changeTeamName,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const changeTeamTag = async (data) => {
    return networkHandler(
        urls.changeTeamTag,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const changeTeamCapitan = async (data) => {
    return networkHandler(
        urls.changeTeamCapitan,
        {
            method: "post",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};



