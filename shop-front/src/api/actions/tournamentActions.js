import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const createTournament = async (data) => {
  return networkHandler(urls.createTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const confirmTournament = async (data) => {
  return networkHandler(urls.confirmTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const tournamentSetup = async (data) => {
  return networkHandler(urls.tournamentSetup, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const tournamentSetupLeave = async (data) => {
  return networkHandler(urls.tournamentSetupLeave, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const tournamentSetupCancel = async (data) => {
  return networkHandler(urls.tournamentSetupCancel, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const tournamentSetupKick = async (data) => {
  return networkHandler(urls.tournamentSetupKick, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const tournamentSetupSet = async (data) => {
  return networkHandler(urls.tournamentSetupSet, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const allTournament = async (data) => {
  return networkHandler(urls.allTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const registerTournament = async (data) => {
  return networkHandler(urls.registerTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const registerCancelTournament = async (data) => {
  return networkHandler(urls.registerCancelTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const acceptTournament = async (data) => {
  return networkHandler(urls.acceptTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const declineTournament = async (data) => {
  return networkHandler(urls.declineTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const inviteTournament = async (data) => {
  return networkHandler(urls.inviteTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const checkinTournment = async (data) => {
  return networkHandler(urls.checkinTournment, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const inviteTournamentCancel = async (data) => {
  return networkHandler(urls.inviteTournamentCancel, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const nextTournament = async (data) => {
  return networkHandler(urls.nextTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const prevTournament = async (data) => {
  return networkHandler(urls.prevTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const profileTournament = async (data) => {
  return networkHandler(urls.profileTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const teamRoster = async (data) => {
  return networkHandler(urls.teamRoster, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
export const joinTournament = async (data) => {
  return networkHandler(urls.joinTournament, {
    method: "post",
    headers: header(true),
    body: JSON.stringify(data),
  });
};
