import * as urls from "../urls";
import { header } from "../header";
import * as constants from "../constants";
import networkHandler from "../fetcher/networkHandler";
import reduxHandler from "../fetcher/reduxHandler";

export const getUser = (data) => {
    return networkHandler(
        urls.getUser,
        {
            method: "POST",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};

export const changePassword = (data) => {
    return networkHandler(
        urls.userChangePassword,
        {
            method: "POST",
            headers: header(true),
            body: JSON.stringify(data)
        }
    )
};
export const getMyInfo = (data, token) => {
    return reduxHandler(
        constants.getMyInfo,
        urls.getUser,
        {
            method: "POST",
            headers: header(true, token),
            body: JSON.stringify(data)
        }
    )
};

