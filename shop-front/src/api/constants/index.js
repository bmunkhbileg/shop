export const gameMode = "GAME_MODE";
export const openModal = "OPEN_MODAL";
export const findingMatch = "FINDING_MATCH";

export const getUser = {
    REQ: "GET_USER_REQ",
    RES: "GET_USER_RES"
};


export const getMyInfo = {
    REQ: "GET_INFO_REQ",
    RES: "GET_INFO_RES"
};
