export default function networkHandler(url, options) {
  let status = null;
  return fetch(url, options)
    .then((res) => {
      status = res.status;
      return res.json();
    })
    .then((json) => {
      return {
        payload: json,
        status: status,
      };
    })
    .catch((err) => {
      return {
        payload: null,
        status: status,
      };
    });
}
