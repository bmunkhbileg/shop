export default function reduxHandler(actionName,url,options){
    return dispatch => {
        let status = null;
        dispatch(requestHandler(null,actionName,"REQ",status));
         fetch(url,options).then((res)=>{
            status = res.status;
            return res.json();
        }).then((json)=>{
            dispatch(requestHandler(json,actionName,"RES",status));
        }).catch((err)=>{
            dispatch(requestHandler(null,actionName,"RES",status));
        })
    }
}

function requestHandler( payload, actionName, requestType, status, requested = null ){
    return {
        type: actionName[requestType],
        status,
        payload,
        requested
    }
}