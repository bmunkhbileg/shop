import { combineReducers } from "redux";
import user from "./user";
import game from "./game";
import modal from "./modal";

const rootReducer = combineReducers({
    user,
    game,
    modal
});

export default rootReducer;