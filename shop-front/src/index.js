import React, { Component } from "react";
import './firebase/config'
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import { createBrowserHistory } from 'history';
import { withRouter } from "react-router-dom";
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Routes from "./Routes";
import './style/style.less'
import 'react-activity/dist/react-activity.css';
import firebase from 'firebase/app';
import 'firebase/firestore'

const options = {
  timeout: 80000,
  position: positions.TOP_CENTER
};

class RootView extends Component {
  render() {
   
    return (
      <Provider template={AlertTemplate} {...options}>
        <React.Fragment >
            <Routes />
        </React.Fragment>
      </Provider>
    );
  }
}

export default withRouter(RootView);

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(
<BrowserRouter>
  <RootView  history={createBrowserHistory}  />
</BrowserRouter>, 
wrapper) : false;